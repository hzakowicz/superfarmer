
rabbit={
    'val':1,
    'name':"Rabbit",
    'number':0,
    'max-number':60
}
sheep={
    'val':6,
    'name':"Sheep",
    'number':0,
    'max-number':24
}
pig={
    'val':12,
    'name':"Pig",
    'number':0,
    'max-number':20
}
cow={
    'val':36,
    'name':"Cow",
    'number':0,
    'max-number':12
}
horse={
    'val':72,
    'name':"Horse",
    'number':0,
    'max-number':60
}
fox={
    'val':-1,
    'name':"Fox",
    'number':0
}
wolf={
    'val':-2,
    'name':"Wolf",
    'number':0
}

s_dog={
    'val':-6,
    'name':"Small dog",
    'number':0,
    'max-number':4
}
B_dog={
    'val':-36,
    'name':"Big dog",
    'number':0,
    'max-number':2
}

dice_animals = [rabbit, sheep, pig, cow, horse, fox, wolf ]

pl_animals = [rabbit, sheep, pig, cow, horse, s_dog, B_dog]
