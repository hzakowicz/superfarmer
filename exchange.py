from display import *
from animals import *


x_rect = 750
y_rect = 120
a_rect = 80
b_rect = 32

rabbit_rect = pygame.Rect(x_rect, y_rect, a_rect, b_rect)
sheep_rect = pygame.Rect(x_rect, y_rect+35, a_rect, b_rect)
pig_rect = pygame.Rect(x_rect, y_rect+70, a_rect, b_rect)
cow_rect = pygame.Rect(x_rect, y_rect+105, a_rect, b_rect)
horse_rect = pygame.Rect(x_rect, y_rect+140, a_rect, b_rect)

animals_rect = [rabbit_rect, sheep_rect, pig_rect, cow_rect, horse_rect]

rabbit_rect2 = pygame.Rect(x_rect, y_rect+220, a_rect, b_rect)
sheep_rect2 = pygame.Rect(x_rect, y_rect+255, a_rect, b_rect)
pig_rect2 = pygame.Rect(x_rect, y_rect+290, a_rect, b_rect)
cow_rect2 = pygame.Rect(x_rect, y_rect+325, a_rect, b_rect)
horse_rect2 = pygame.Rect(x_rect, y_rect+360, a_rect, b_rect)
sdog_rect2 = pygame.Rect(x_rect, y_rect+395, a_rect, b_rect)
Bdog_rect2 = pygame.Rect(x_rect, y_rect+430, a_rect, b_rect)

animals_rect2 = [rabbit_rect2, sheep_rect2, pig_rect2, cow_rect2, horse_rect2, sdog_rect2, Bdog_rect2]


rabbit_in=False
sheep_in=False
pig_in=False
cow_in=False
horse_in=False

animals_in = [rabbit_in, sheep_in, pig_in, cow_in, horse_in]

rabbit_in2=False
sheep_in2=False
pig_in2=False
cow_in2=False
horse_in2=False
sdog_in2=False
Bdog_in2=False

animals_in2 = [rabbit_in2, sheep_in2, pig_in2, cow_in2, horse_in2, sdog_in2, Bdog_in2]


rabbit_num=''
sheep_num=''
pig_num=''
cow_num=''
horse_num=''

animals_num = [rabbit_num, sheep_num, pig_num, cow_num, horse_num]

rabbit_num2=''
sheep_num2=''
pig_num2=''
cow_num2=''
horse_num2=''
sdog_num2=''
Bdog_num2=''

animals_num2 = [rabbit_num2, sheep_num2, pig_num2, cow_num2, horse_num2, sdog_num2, Bdog_num2]


def exchange_static(screen):
    draw_text("Give:", font, blue, 750, 90, screen)

    draw_text("Rabbit", font, white, 850, y_rect+5, screen)
    draw_text("Sheep", font, white, 850, y_rect+40, screen)
    draw_text("Pig", font, white, 850, y_rect+75, screen)
    draw_text("Cow", font, white, 850, y_rect+110, screen)
    draw_text("Horse", font, white, 850, y_rect+145, screen)

    draw_text("Get:", font, blue, 750, 310, screen)

    draw_text("Rabbit", font, white, 850, y_rect+225, screen)
    draw_text("Sheep", font, white, 850, y_rect+260, screen)
    draw_text("Pig", font, white, 850, y_rect+295, screen)
    draw_text("Cow", font, white, 850, y_rect+330, screen)
    draw_text("Horse", font, white, 850, y_rect+365, screen)
    draw_text("Small dog", font, white, 850, y_rect+400, screen)
    draw_text("Big dog", font, white, 850, y_rect+435, screen)


def exchange_table(screen):
    ex_rect = pygame.Rect(250, 440, 465, 110)
    pygame.draw.rect(screen,blue,ex_rect,2)

    draw_text("6 rabbits = 1 sheep", font, white, 265, 455, screen)
    draw_text("2 sheeps = 1 pig", font, white, 265, 485, screen)
    draw_text("3 pigs = 1 cow", font, white, 265, 515, screen)

    draw_text("2 cow = 1 horse", font, white, 490, 455, screen)
    draw_text("1 small dog = 1 sheep", font, white, 490, 485, screen)
    draw_text("1 big dog = 1 cow", font, white, 490, 515, screen)


def exchange_check(animals_num, animals_num2):
    for i in range(len(animals_num)):
        if animals_num[i] == '':
            animals_num[i] = 0
        else:
            animals_num[i] = int(animals_num[i])

    for i in range(len(animals_num2)):
        if animals_num2[i] == '':
            animals_num2[i] = 0
        else:
            animals_num2[i] = int(animals_num2[i])

    sumin = 0
    sumout = 0
    for i in range(len(animals_num)):
        sumin += (animals_num[i]*pl_animals[i]['val'])
    
    for i in range(len(animals_num2)):
        sumout += abs((animals_num2[i]*pl_animals[i]['val']))

    if sum(animals_num) <= 0 or sum(animals_num2) <= 0:
        return False

    else:
        if sumin == sumout:
            return True
        else:
            return False


def exchange_add(animals_num, animals_num2):
    for i in range(len(animals_num2)):
        if animals_num2[i]>0:
            pl_animals[i]['number'] += int(animals_num2[i])

    for i in range(len(animals_num)):
        if animals_num[i]>0:
            pl_animals[i]['number'] -= int(animals_num[i])
            

def restart_ex():
    for i in range(len(animals_num)):
        animals_num[i]=''
    for i in range(len(animals_num2)):
        animals_num2[i]=''


def finish():
    for i in range(len(pl_animals)-2):
        if pl_animals[i]['number']<=0:
            return False
    return True
