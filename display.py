import pygame

pygame.init()

WIDTH = 1000
HEIGHT = 600
FPS=60

black=(0,0,0)
white=(255,255,255)
pink=(255, 0, 255)
blue=(0,255,255)
gray=(77,77,77)

font = pygame.font.Font(None, 30)

color_active=blue
color_passive=gray
color=color_passive



def draw_text(text, font, text_col, x, y, screen):
    text_dis = font.render(text, True, text_col)
    screen.blit(text_dis, (x,y)) 


def hCenter_text(text, font, text_col, width, y, screen):
    text_dis = font.render(text, True, text_col)
    text_rect = text_dis.get_rect(center=(width/2, y))
    screen.blit(text_dis, text_rect)


def text_render(active, user_text, input_rect, screen):
    if active:
        color=color_active
    else:
        color=color_passive
    pygame.draw.rect(screen,color,input_rect,2)

    text_surface=font.render(user_text, True,(255,255,255))
    screen.blit(text_surface,(input_rect.x+5,input_rect.y+5))

    # input_rect.w=max(100,text_surface.get_width()+10)

