from animals import *
from roll_animals import add_animals, take_animals
from exchange import *



def test_add_animals():
    a = 0   # królik
    b = 1   # owca
    dice_animals[a]['number'] = 7
    dice_animals[b]['number'] = 4
    add_animals(a,b)

    assert dice_animals[a]['number'] == 11 and dice_animals[b]['number'] == 6

    x = 3  # krowa
    y = 2   # świnia
    dice_animals[x]['number'] = 0
    dice_animals[y]['number'] = 3
    add_animals(x,y)

    assert dice_animals[y]['number'] == 5 and dice_animals[5]['number'] == 0


def test_take_animals():
    a = 5  #lis
    b = 2
    dice_animals[0]['number'] = 24
    take_animals(a,b)
    assert dice_animals[0]['number'] == 1

    dice_animals[0]['number'] = 10
    pl_animals[5]['number'] = 1
    take_animals(a,b)
    assert dice_animals[0]['number'] == 10 and pl_animals[5]['number'] == 0

    a = 0
    b = 6
    dice_animals[3]['number'] = 3
    take_animals(a,b)
    assert dice_animals[3]['number'] == 0

    dice_animals[3]['number'] = 5
    pl_animals[6]['number'] = 1
    take_animals(a,b)
    assert dice_animals[3]['number'] == 5 and pl_animals[6]['number'] == 0


def test_exchange_check():
    animals_num[0] = -5
    assert exchange_check(animals_num, animals_num2) == False

    animals_num[0] = 7
    animals_num2[1] = 1
    assert exchange_check(animals_num, animals_num2) == False

    animals_num[0] = 6
    animals_num2[1] = 1
    assert exchange_check(animals_num, animals_num2) == True

    animals_num[1] = 1
    animals_num2[0] = 6
    assert exchange_check(animals_num, animals_num2) == True


def test_exchange_add():
    pl_animals[0]['number'] = 13
    animals_num[0] = 6

    pl_animals[1]['number'] = 2
    animals_num2[1] = 1
    exchange_check(animals_num, animals_num2)
    exchange_add(animals_num, animals_num2)

    assert pl_animals[0]['number'] == 7 and pl_animals[1]['number'] == 3

    pl_animals[2]['number'] = 3
    animals_num[2] = 2

    pl_animals[1]['number'] = 6
    animals_num2[1] = 4
    exchange_check(animals_num, animals_num2)
    exchange_add(animals_num, animals_num2)

    assert pl_animals[2]['number'] == 1 and pl_animals[1]['number'] == 10