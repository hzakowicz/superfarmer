import random
from animals import *
from display import *

# dice_animals = [rabbit, sheep, pig, cow, horse, fox, wolf]
# kostki zawierają indeksy zwierząt
dice1=[0,0,0,0,0,0,1,1,1,2,4,6]
dice2=[0,0,0,0,0,0,1,1,2,2,3,5]

def roll():
    x = random.choice(dice1)
    y = random.choice(dice2)
    return x,y


def draw_roll(anim1_i, anim2_i,screen, width):
    anim1_name = dice_animals[anim1_i]['name']
    anim2_name = dice_animals[anim2_i]['name']

    hCenter_text(anim1_name, font, white, width, 150, screen)
    hCenter_text(anim2_name, font, white, width, 180, screen)


# test czy x, y z roll zgadza się z draw_roll ??


def add_animals(anim1_i, anim2_i):
    number0 = 0 
    number1 = 0
    number2 = 0
    if anim1_i == anim2_i:
        number0 = dice_animals[anim1_i]['number'] + 2
    elif anim1_i != anim2_i:
        number1 = dice_animals[anim1_i]['number'] + 1
        number2 = dice_animals[anim2_i]['number'] + 1
    else:
        number0 = 0
        number1 = 0
        number2 = 0

    if number0 > 0:
        dice_animals[anim1_i]['number'] += (number0//2)
    elif number1 > 0 or number2 > 0:
        dice_animals[anim1_i]['number'] += (number1//2)
        dice_animals[anim2_i]['number'] += (number2//2)



def take_animals(anim1_i, anim2_i):
    if anim1_i == 6 or anim2_i == 6:
        if pl_animals[6]['number'] == 1:  # czy ma dużego psa
            pl_animals[6]['number'] = 0  # zabiera psa
        else:                           # zabiera zwierzęta
            dice_animals[1]['number'] = 0  # owce
            dice_animals[2]['number'] = 0  # świnie
            dice_animals[3]['number'] = 0  # krowy
    if anim1_i == 5 or anim2_i == 5:
        if pl_animals[5]['number'] == 1:  # czy ma małego psa
            pl_animals[5]['number'] = 0
        else:
            dice_animals[0]['number'] = 1  # króliki, bez 1


def display_pl_animals(screen):  #  (player)
    x = 60
    y = 20
    draw_text("YOUR ANIMALS", font, blue, x, y+10, screen)
    for i in range(len(pl_animals)):
        y += 50
        name = pl_animals[i]['name']
        number = str(pl_animals[i]['number'])

        draw_text(name, font, white, x, y, screen)
        draw_text(number, font, white, x+120, y, screen)


def show_points(points, screen):
    draw_text("Number of dice rolls: ", font, white, 320, 30, screen)

    draw_text(str(points)+" points", font, pink, 550, 30, screen)

