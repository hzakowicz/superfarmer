import pygame_gui, sys
from display import *
from exchange import *
from roll_animals import roll, draw_roll, display_pl_animals, add_animals, take_animals, show_points
from menu_lead import menu, add_leader, restart_animals


screen = pygame.display.set_mode((WIDTH,HEIGHT))
pygame.display.set_caption("Superfarmer")
clock = pygame.time.Clock()
manager = pygame_gui.UIManager((WIDTH,HEIGHT))

# gui elements
# menu
name_input = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((0,-110),(200,40)), manager=manager, object_id="#name_input", anchors={'center':'center'})

start_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((0, -50), (-1,-1)), text="START", manager=manager, anchors={'center':'center'})

#game
menu_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((20, 550), (-1, -1)), text="MENU", manager=manager)

roll_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((0, -200), (-1, -1)), text="ROLL THE DICE", manager=manager, anchors={'center':'center'})

exchange_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((310, -240), (-1, -1)), text="EXCHANGE", manager=manager, object_id="#exchange_button", anchors={'center':'center'})

confirm_button = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((400, -240), (-1, -1)), text="CONFIRM", manager=manager, object_id="#confirm_button", anchors={'center':'center'})


def game_start():

    menu_bg = pygame.image.load("bg_img.jpg").convert_alpha()
    menu_bg = pygame.transform.scale(menu_bg, (WIDTH, HEIGHT))
    game_bg = menu_bg.copy()

    # pl_animals[0]['number']=1
    # pl_animals[1]['number']=1
    # pl_animals[2]['number']=1
    # pl_animals[6]['number']=1

    noname = False
    start = False
    if_roll = False
    if_exchange = False
    if_finish = False
    failed_ex = False

    gamerun = True
    while gamerun:

        refresh_rate = clock.tick(FPS)/1000
        screen.fill(black)

        if not start:
            screen.blit(menu_bg, (0, 0))
            menu(screen)
            start_button.show()
            name_input.show()
            menu_button.hide()
            roll_button.hide()
            exchange_button.hide()
            confirm_button.hide()
            if noname:
                draw_text("<-- Podaj nazwę", font, blue, 610, 180, screen)

        else:
            game_bg.set_alpha(70)
            screen.blit(game_bg, (0, 0))
            menu_button.show()
            roll_button.show()
            exchange_button.show()
            confirm_button.show()
            start_button.hide()
            name_input.hide()

            if if_roll:
                draw_roll(anim1_i, anim2_i, screen, WIDTH)

            display_pl_animals(screen)
            show_points(points, screen)
            exchange_table(screen)

            if if_exchange:
                exchange_static(screen)

                for i in range(len(animals_rect)):
                    text_render(animals_in[i], str(animals_num[i]), animals_rect[i], screen)

                for i in range(len(animals_rect2)):
                    text_render(animals_in2[i], str(animals_num2[i]), animals_rect2[i], screen)

            if failed_ex:
                draw_text("Incorrect input for exchange", font, pink, 700, 20, screen)

            if if_finish:
                hCenter_text("WIN", pygame.font.Font(None,80), pink, WIDTH, 250, screen)
                hCenter_text(str(points)+"points!", pygame.font.Font(None,60), pink, WIDTH, 310, screen)


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame_gui.UI_BUTTON_START_PRESS:
                if event.ui_element == start_button:
                    points=0
                    name = name_input.get_text()
                    if name == '':
                        noname = True
                    else:
                        start = True

                if event.ui_element == menu_button:
                    start = False
                    if_finish == False
                    restart_animals(pl_animals)

                if event.ui_element == exchange_button:
                    if_exchange = True

                if event.ui_element == confirm_button:
                    if_finish = finish()
                    if if_finish:
                        add_leader(points,name)
                    else:
                        good_exchange = exchange_check(animals_num, animals_num2)
                        if good_exchange:
                            exchange_add(animals_num, animals_num2)
                            failed_ex = False
                        else:
                            failed_ex = True
                    if_exchange = False
                    restart_ex()

                if event.ui_element == roll_button:
                    if_roll = True
                    if_exchange = False

                    points += 1

                    anim1_i, anim2_i = roll()
                    if anim1_i == 6 or anim2_i == 5:
                        take_animals(anim1_i, anim2_i)
                    else:
                        add_animals(anim1_i, anim2_i)
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                for i, rect in enumerate(animals_rect):
                    if rect.collidepoint(event.pos):
                        animals_in[i] = True
                    else:
                        animals_in[i] = False

                for i, rect2 in enumerate(animals_rect2):
                    if rect2.collidepoint(event.pos):
                        animals_in2[i] = True
                    else:
                        animals_in2[i] = False
            
            for i, animal_in in enumerate(animals_in):
                if event.type == pygame.KEYDOWN and animal_in:
                    if event.key == pygame.K_BACKSPACE:
                        animals_num[i] = animals_num[i][0:-1]
                    else:
                        animals_num[i] += event.unicode

            for i, animal_in in enumerate(animals_in2):
                if event.type == pygame.KEYDOWN and animal_in:
                    if event.key == pygame.K_BACKSPACE:
                        animals_num2[i] = animals_num2[i][0:-1]
                    else:
                        animals_num2[i] += event.unicode
                

        manager.process_events(event)
        manager.update(refresh_rate)
        manager.draw_ui(screen)

        pygame.display.flip()


game_start()
