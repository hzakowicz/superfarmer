from display import *


def menu(screen):
    hCenter_text("SUPERFARMER", pygame.font.Font(None, 60), (9,32,37,255), WIDTH, 40, screen)
    # hCenter_text("- SOLO -", font, blue, WIDTH, 85, screen)

    hCenter_text("player name:", font, white, WIDTH, 140, screen)

    hCenter_text("Leaderboard:", font, pink, WIDTH, 300, screen)

    with open("leaderboard.txt", "r") as f:
        tab = f.readlines()

    y=300
    for i in range(len(tab)):
        y+=35
        hCenter_text(tab[i], font, white, WIDTH, y, screen)


def read_leader(screen):
    with open("leaderboard.txt", "r") as f:
        tab = f.readlines()

    y=300
    for i in range(len(tab)):
        y+=35
        hCenter_text(tab[i], font, black, WIDTH, 300+y, screen) 
        

def add_leader(points, name):
    with open("leaderboard.txt", "r") as f:
        tab = f.readlines()

    tab.append(f"{points} {name}"+"\n")

    tab = sorted(tab)

    with open("leaderboard.txt", "w") as f2:
        f2.write("".join(tab)+"\n")


def restart_animals(pl_animals):
    for i in range(len(pl_animals)):
        pl_animals[i]['number']=0